#!/bin/sh

rm -f mount-path.conf

declare -A PATHS
PATHS=(
  [html]=/var/www/html
  [httpd]=/var/log/httpd
  [conf.d]=/etc/httpd/conf.d
)

echo "Enlaces sugeridos"

for KEY in "${!PATHS[@]}"
  do
    read -p "Desea configurar el enlace para ${PATHS[$KEY]} (S|N):" OPTION
    if [[ $OPTION = 'S' || $OPTION = 's' ]]; then
      read -e -p " * ${PATHS[$KEY]} => " -i "$KEY"
      echo "$KEY=${PATHS[$KEY]}" >> mount-path.conf
    fi
  done
