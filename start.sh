#!/bin/sh

clear
echo "########################################################"
echo "# Script para configuración de directorios compartidos #"
echo "########################################################"
echo ""
echo "Seleccione el nombre del directorio compartido a enlazar:"

USER=`whoami`

echo "Se agrega el usuario al grupo de VBox"

usermod -a -G vboxsf "$USER"

echo "Eliminado la expiración de contraseña para el usuario [$USER]"

chage -m 0 -M 99999 -I -1 -E -1 "$USER"

echo "Predefiniendo la codificación de caracteres de PHP como ISO8859-1"

sed -i 's/default_charset\s*=\s*"[^"]*"/default_charset = "ISO8859-1"/g' /etc/php.ini
sed -i 's/default_charset\s*=\s*"[^"]*"/default_charset = "ISO8859-1"/g' /etc/opt/rh/rh-php71/php.ini

echo "Restaurando la configuración básica de montado de volumenes para el SO"

rm -f /etc/profile.d/mount-vboxsf.sh
rm -f /etc/profile.d/mount-path.conf

VBOXADDITION=$(lsmod | grep -i vbox)

if [[ $VBOXADDITION = '' ]]; then
    ls /opt/VBoxGuestAdditions-*/init
    if [[ $? -ne 0 ]]; then
        echo "No se ha localizado el montado para la imagen VBOXGuestAdditions"
        exit 1
    else
        echo "Instalando VBoxGuestAdditions"
        cd /opt/VBoxGuestAdditions-*/init
        ./vboxadd setup
    fi
fi

sh set-mount-vboxsf.sh

chmod +x mount-vboxsf.sh

rm -rf /etc/profile.d/mount-vboxsf.sh /etc/profile.d/mount-path.conf

if [ -f "mount-path.conf" ]; then
    ln mount-vboxsf.sh /etc/profile.d/mount-vboxsf.sh
    ln mount-path.conf /etc/profile.d/mount-path.conf
fi

echo "El sistema se reiniciara en 1 minuto"
shutdown -r 1