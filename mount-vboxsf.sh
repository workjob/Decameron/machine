#!/bin/sh

systemctl stop httpd24-httpd

CURRENT_PATH=$(dirname -- $0)

if [ ! -f "$file" ]; then
  CURRENT_PATH=/etc/profile.d
fi

if [ -f "$CURRENT_PATH/mount-path.conf" ]; then

  echo "Se estan cargando las configuraciones desde $CURRENT_PATH/mount-path.conf"

  declare -A PATHS

  while IFS="=" read -r KEY VALUE
    do
      PATHS[$KEY]=$VALUE
    done < $CURRENT_PATH/mount-path.conf  

  echo "Montando las unidades configuradas"

  for KEY in "${!PATHS[@]}"
    do
      echo " * ${PATHS[$KEY]} => $KEY"
      MESSAGE=`umount -t vboxsf $KEY 2>&1`
      STATUS=$?
      if [[ $STATUS -ne 0 ]]; then
        rm -rf ${PATHS[$KEY]}/*
      fi
      MESSAGE=`mount -t vboxsf $KEY ${PATHS[$KEY]} 2>&1`
    done
  
fi

if [ ! -f /var/log/httpd/coredump ]; then
  mkdir -p /var/log/httpd/coredump
fi

systemctl start httpd24-httpd